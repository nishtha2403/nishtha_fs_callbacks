const fs = require('fs');
const path = require('path');

function createAndDeleteFiles(dirPath,n=3){

    fs.mkdir(dirPath, (err) => {
        if(err){
            console.error(err);
        } else {
            let counter = 0;

            for(let index=1;index<=n;index++){
                let fileName = 'random' +  index + '.json';

                fs.writeFile(path.join(dirPath,fileName),'utf-8', (err) => {
                    if(err){
                        console.error(err);
                    } else {
                       counter += 1;

                       if(counter === n){
                            for(let index= 1;index<=n;index++){
                                let fileName = 'random' + index + '.json';
                                
                                fs.unlink(path.join(dirPath,fileName), (err) =>{
                                    if(err){
                                        console.error(err);
                                    } else {
                                        console.log(`${fileName} deleted !`);
                                    }
                                });
                            }
                       }
                    }
                });
            }
        }
    });
}

module.exports =  createAndDeleteFiles;

