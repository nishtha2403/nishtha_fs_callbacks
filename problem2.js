const fs = require('fs');
const path = require('path');

function doFileReadWrite(filePath){

    let fileExt = path.extname(filePath);
    let fileName = path.basename(filePath,fileExt);

    fs.mkdir(path.join('output'), (err) => {
       if( err !== null && err.code !==  'EEXIST') {
           console.error(err);
       } else {
            fs.readFile(filePath,'utf-8',(err,data) => {
                if(err) {
                    console.error(err);
                } else {
                    let dataInUpperCase = data.toUpperCase();
                    let fileNameUC = fileName + 'UpperCase' + fileExt;
    
                    fs.writeFile(path.join('output', `${fileNameUC}`),dataInUpperCase,(err) => {
                        if(err) {
                            console.error(err);
                        } else {
                            fs.writeFile(path.join('output','filenames.txt'),`${fileNameUC}`,(err) => {
                                if(err) {
                                    console.error(err);
                                } 
                            });
    
                            fs.readFile(path.join('output',`${fileNameUC}`),'utf-8',(err,data) => {
                                if(err) {
                                    console.error(err);
                                } else {
                                    let dataInLowerCase = data.toLowerCase();
                                    let fileNameLC = fileName + 'LowerCase' + fileExt;
                            
                                    fs.writeFile(path.join('output',`${fileNameLC}`),dataInLowerCase,(err) => {
                                        if(err) {
                                            console.error(err);
                                        } else {
                                            fs.appendFile(path.join('output','filenames.txt'),`\n${fileNameLC}`,(err) => {
                                                if(err) {
                                                    console.error(err);
                                                } 
                                            });
        
                                            fs.readFile(path.join('output',`${fileNameLC}`), 'utf-8',(err,data) => {
                                                if(err) {
                                                    console.error(err);
                                                } else {
                                                    let sortedData = data.split(' ').sort().join(' ');
                                                    let fileNameSorted = fileName + 'Sorted' + fileExt;
        
                                                    fs.writeFile(path.join('output',`${fileNameSorted}`),sortedData,(err) => {
                                                        if(err) {
                                                            console.error(err);
                                                        } else {
                                                            fs.appendFile(path.join('output','filenames.txt'),`\n${fileNameSorted}`,(err) => {
                                                                if(err) {
                                                                    console.error(err);
                                                                } else {
                                                                    fs.readFile(path.join('output','filenames.txt'),'utf-8',(err,data) => {
                                                                        if(err) {
                                                                            console.error(err);
                                                                        } else {
                                                                            let dataArray = data.split('\n');
                
                                                                            dataArray.forEach((filename) => {
                                                                                fs.unlink(path.join('output',filename), () => {
                                                                                    console.log(`${filename} deleted!`);
                                                                                });
                                                                            });
                                                                         }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

module.exports = doFileReadWrite;